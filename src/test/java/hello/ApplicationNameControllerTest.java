package hello;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ApplicationNameControllerTest {
    @Test
    public void shouldReturnApplicationName(){
        ApplicationNameController applicationNameController = new ApplicationNameController();
        ApplicationName applicationName = applicationNameController.applicationName();
        assertEquals("BookApplication", applicationName.getName());
    }
}
