package hello;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ApplicationNameController {

    @RequestMapping("/applicationName")
    public ApplicationName applicationName() {
        return new ApplicationName("BookApplication");
    }
}
