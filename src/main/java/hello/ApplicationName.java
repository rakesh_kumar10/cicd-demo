package hello;

public class ApplicationName {
    private String name;

    public ApplicationName(String name) {

        this.name = name;
    }

    public String getName() {
        return name;
    }
}
